package edu.twitter.connector.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import edu.twitter.connector.model.TweetsQuery;
import edu.twitter.connector.service.TweetHarvesterService;
import edu.twitter.connector.service.TweetStoreService;


public class TweetHarvesterServiceImpl implements TweetHarvesterService {

  private static final Logger logger = LoggerFactory
      .getLogger(TweetHarvesterServiceImpl.class);
  @Resource
  private TwitterStream twitterStream;
  @Resource
  private MongoClient mongo;
  @Resource
  private TweetStoreService tweetStoreService;
  private DBCollection twitterCollection;
  private boolean useSpringData;
  
  private int count = 0;
  
  /**
   * Constructor.
   * 
   * @param useSpringData whether to use Spring data.
   */
  public TweetHarvesterServiceImpl(boolean useSpringData) {
    this.useSpringData = useSpringData;
  }

  public void startTwitterStream(TweetsQuery tweetsQuery) {
    intializeDbCollection(tweetsQuery.getDbName());
    twitterStream.addListener(getStatusListener());
    twitterStream.filter(getFilterQuery(tweetsQuery.getKeywords()));
  }

  public void stopTwitterStream() {
    this.twitterStream.shutdown();
    logger.info("The twitter stream has been shutdown successfully.");
  }

  private static FilterQuery getFilterQuery(List<String> keywords) {
    FilterQuery fq = new FilterQuery();
    String[] searchTerms = (String[]) keywords.toArray(new String[keywords.size()]);
    fq.track(searchTerms);

    String[] languages = { "en" };
    fq.language(languages);

    return fq;
  }

  private void intializeDbCollection(String dbName) {
    if (dbName == null || "".equals(dbName)) {
      dbName = "example1";
    }
    
    if (!useSpringData) {
      DB db = this.mongo.getDB(dbName);
      this.twitterCollection = db.getCollection("tweets");
      BasicDBObject index = new BasicDBObject("tweetId", Integer.valueOf(1));
      this.twitterCollection.ensureIndex(index, new BasicDBObject("unique",
          Boolean.valueOf(true)));
    }
  }

  private StatusListener getStatusListener() {
     return new StatusListener() {
      public void onException(Exception ex) {
        logger.error("Twitter stream error.", ex);
      }

      public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
        logger.info("Tract limitation notice: " + numberOfLimitedStatuses);
      }

      public void onStatus(Status status) {
        
        if (count % 100 == 0) {
          logger.info("There are {} tweets in the DB.", count);
        }
        
        if (useSpringData) {
          saveTweetWithSpringMongoData(status);
        } else {
          saveTweetWithMongoClient(status);
        }
        
        count++;

      }
      
      private void saveTweetWithSpringMongoData(Status status) {
        tweetStoreService.storeTweetInDb(status);
      }
      
      private void saveTweetWithMongoClient(Status status) {
        BasicDBObject object = new BasicDBObject();
        object.append("tweetId", Long.valueOf(status.getId()));
        object.append("tweetTimestamp", status.getCreatedAt());
        object.append("tweetLanguageCode", status.getIsoLanguageCode());
        if (status.getGeoLocation() != null) {
          object.append("tweetCoordinates", new BasicDBObject("latitude",
              Double.valueOf(status.getGeoLocation().getLatitude())).append(
              "longitude",
              Double.valueOf(status.getGeoLocation().getLongitude())));
        }
        object.append("tweetText", status.getText());
        if (status.getHashtagEntities().length != 0) {
          BasicDBList hashtagEntities = new BasicDBList();
          for (int i = 0; i < status.getHashtagEntities().length; i++) {
            hashtagEntities.add(new BasicDBObject("start", Integer
                .valueOf(status.getHashtagEntities()[i].getStart()))
                .append("end",
                    Integer.valueOf(status.getHashtagEntities()[i].getEnd()))
                .append("text", status.getHashtagEntities()[i].getText()));
          }
          object.append("hashtagEntities", hashtagEntities);
        }
        if (status.getMediaEntities().length != 0) {
          BasicDBList mediaEntities = new BasicDBList();
          for (int i = 0; i < status.getMediaEntities().length; i++) {
            mediaEntities.add(new BasicDBObject("start", Integer.valueOf(status
                .getMediaEntities()[i].getStart())).append("end",
                Integer.valueOf(status.getMediaEntities()[i].getEnd())).append(
                "text", status.getMediaEntities()[i].getText()));
          }
          object.append("mediaEntities", mediaEntities);
        }
        if (status.getSymbolEntities().length != 0) {
          BasicDBList symbolEntities = new BasicDBList();
          for (int i = 0; i < status.getSymbolEntities().length; i++) {
            symbolEntities.add(new BasicDBObject("start", Integer
                .valueOf(status.getSymbolEntities()[i].getStart())).append(
                "end", Integer.valueOf(status.getSymbolEntities()[i].getEnd()))
                .append("text", status.getSymbolEntities()[i].getText()));
          }
          object.append("symbolEntities", symbolEntities);
        }
        if (status.getURLEntities().length != 0) {
          BasicDBList urlEntities = new BasicDBList();
          for (int i = 0; i < status.getURLEntities().length; i++) {
            urlEntities.add(new BasicDBObject("start", Integer.valueOf(status
                .getURLEntities()[i].getStart())).append("end",
                Integer.valueOf(status.getURLEntities()[i].getEnd())).append(
                "text", status.getURLEntities()[i].getText()));
          }
          object.append("urlEntities", urlEntities);
        }
        if (status.getUserMentionEntities().length != 0) {
          BasicDBList userMentionEntities = new BasicDBList();
          for (int i = 0; i < status.getUserMentionEntities().length; i++) {
            userMentionEntities
                .add(new BasicDBObject("start", Integer.valueOf(status
                    .getUserMentionEntities()[i].getStart()))
                    .append(
                        "end",
                        Integer.valueOf(status.getUserMentionEntities()[i]
                            .getEnd())).append("text",
                        status.getUserMentionEntities()[i].getText()));
          }
          object.append("userMentionEntities", userMentionEntities);
        }
        object.append(
            "userData",
            new BasicDBObject("userName", status.getUser().getName()).append(
                "userFollowersCount",
                Integer.valueOf(status.getUser().getFollowersCount())).append(
                "retweetCount", Integer.valueOf(status.getRetweetCount())));

        twitterCollection.insert(new DBObject[] { object });
      }

      public void onStallWarning(StallWarning warning) {
        logger.warn("Code" + warning.getCode() + ", Message: "
            + warning.getMessage() + ", Percent full: "
            + warning.getPercentFull());
      }

      public void onScrubGeo(long userId, long upToStatusId) {
        logger.warn("There is a warning to delete geo information.");
      }

      public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        logger.warn(
            "Deletion notice, userId: " + statusDeletionNotice.getUserId(),
            ", statusId: " + statusDeletionNotice.getStatusId());
      }
    };
  }

}
