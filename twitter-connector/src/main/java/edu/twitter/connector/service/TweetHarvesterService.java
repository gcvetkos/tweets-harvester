package edu.twitter.connector.service;

import edu.twitter.connector.model.TweetsQuery;


public interface TweetHarvesterService {
  
  void startTwitterStream(TweetsQuery paramTweetsQuery);
  
  void stopTwitterStream();

}
