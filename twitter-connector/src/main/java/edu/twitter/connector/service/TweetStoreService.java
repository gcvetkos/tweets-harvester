package edu.twitter.connector.service;

import twitter4j.Status;

public interface TweetStoreService {
  
  void storeTweetInDb(Status status);

}
