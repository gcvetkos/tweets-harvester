package edu.twitter.connector.service.impl;

import javax.annotation.Resource;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import twitter4j.Status;
import edu.twitter.connector.model.Tweet;
import edu.twitter.connector.model.TweetCoordinates;
import edu.twitter.connector.model.TweetEntity;
import edu.twitter.connector.model.TweetUserData;
import edu.twitter.connector.service.TweetStoreService;

@Service
public class TweetStoreServiceImpl implements TweetStoreService {
  
  @Resource
  private MongoTemplate mongoTemplate;

  @Override
  public void storeTweetInDb(Status status) {
    Tweet tweet = new Tweet();
    tweet.setTweetId(Long.valueOf(status.getId()));
    tweet.setTweetTimestamp(status.getCreatedAt());
    tweet.setTweetLanguageCode(status.getIsoLanguageCode());
    if (status.getGeoLocation() != null) {
      TweetCoordinates tweetCoordinates = new TweetCoordinates();
      tweetCoordinates.setLatitude(status.getGeoLocation().getLatitude());
      tweetCoordinates.setLongitude(status.getGeoLocation().getLongitude());
      tweet.setTweetCoordinates(tweetCoordinates);
    }
    tweet.setTweetText(status.getText());
    if (status.getHashtagEntities().length != 0) {
      for (int i = 0; i < status.getHashtagEntities().length; i++) {
        TweetEntity entity = new TweetEntity();
        entity.setStart(status.getHashtagEntities()[i].getStart());
        entity.setEnd(status.getHashtagEntities()[i].getEnd());
        entity.setText(status.getHashtagEntities()[i].getText());
        tweet.addHashtagEntity(entity);
      }
    }
    if (status.getMediaEntities().length != 0) {
      for (int i = 0; i < status.getMediaEntities().length; i++) {
        TweetEntity entity = new TweetEntity();
        entity.setStart(status.getMediaEntities()[i].getStart());
        entity.setEnd(status.getMediaEntities()[i].getEnd());
        entity.setText(status.getMediaEntities()[i].getText());
        tweet.addMediaEntity(entity);
      }
    }
    if (status.getSymbolEntities().length != 0) {
      for (int i = 0; i < status.getSymbolEntities().length; i++) {
        TweetEntity entity = new TweetEntity();
        entity.setStart(status.getSymbolEntities()[i].getStart());
        entity.setEnd(status.getSymbolEntities()[i].getEnd());
        entity.setText(status.getSymbolEntities()[i].getText());
        tweet.addSymbolEntity(entity);
      }
    }
    if (status.getURLEntities().length != 0) {
      for (int i = 0; i < status.getURLEntities().length; i++) {
        TweetEntity entity = new TweetEntity();
        entity.setStart(status.getURLEntities()[i].getStart());
        entity.setEnd(status.getURLEntities()[i].getEnd());
        entity.setText(status.getURLEntities()[i].getText());
        tweet.addUrlEntity(entity);
      }
    }
    if (status.getUserMentionEntities().length != 0) {
      for (int i = 0; i < status.getUserMentionEntities().length; i++) {
        TweetEntity entity = new TweetEntity();
        entity.setStart(status.getUserMentionEntities()[i].getStart());
        entity.setEnd(status.getUserMentionEntities()[i].getEnd());
        entity.setText(status.getUserMentionEntities()[i].getText());
        tweet.addUserMentionEntity(entity);
      }
    }
    
    TweetUserData userData = new TweetUserData();
    userData.setUserName(status.getUser().getName());
    userData.setUserFollowersCount(status.getUser().getFollowersCount());
    userData.setRetweetCount(status.getRetweetCount());
    tweet.setUserData(userData);
    
    // apply appropriate filters and transofrmations
    
    mongoTemplate.save(tweet);
  }
  
}
