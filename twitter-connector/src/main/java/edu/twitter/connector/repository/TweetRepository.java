package edu.twitter.connector.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.twitter.connector.model.Tweet;

@Repository
public interface TweetRepository extends MongoRepository<Tweet, String> {
  
//  /**
//   * Find a user by its username.
//   * 
//   * @param username
//   * @return
//   */
//  public UserDetail findByUsername(String username);

}
