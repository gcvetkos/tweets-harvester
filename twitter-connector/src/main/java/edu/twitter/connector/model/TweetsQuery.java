package edu.twitter.connector.model;

import java.util.ArrayList;
import java.util.List;

public class TweetsQuery {
  
  private List<String> keywords;
  private String dbName;

  public TweetsQuery() {
    this.keywords = new ArrayList<String>();
    this.dbName = "tq";
  }

  public TweetsQuery(List<String> keywords, String dbName) {
    this.keywords = keywords;
    this.dbName = dbName;
  }

  public List<String> getKeywords() {
    return this.keywords;
  }

  public String getDbName() {
    return this.dbName;
  }

}
