package edu.twitter.connector.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document(collection="tweets")
public class Tweet {

  @Id
  private String id;
  @Indexed(dropDups = true, unique = true, background = true)
  private Long tweetId;
  @Indexed(background = true)  
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date tweetTimestamp;
  private String tweetLanguageCode;
  private TweetCoordinates tweetCoordinates;
  private String tweetText;
  private String tweetTextWithoutStopWords;
  private List<String> lemmatizedTweetWords;
  @Indexed(background = true)
  private List<TweetEntity> hashtagEntities;
  private List<TweetEntity> mediaEntities;
  private List<TweetEntity> symbolEntities;
  private List<TweetEntity> urlEntities;
  private List<TweetEntity> userMentionEntities;
  private TweetUserData userData;
  
  public void addHashtagEntity(TweetEntity hashtagEntity) {
    if (hashtagEntities == null) {
      hashtagEntities = new ArrayList<>();
    }
    hashtagEntities.add(hashtagEntity);
  }
  
  public void addMediaEntity(TweetEntity mediaEntity) {
    if (mediaEntities == null) {
      mediaEntities = new ArrayList<>();
    }
    mediaEntities.add(mediaEntity);
  }
  
  public void addSymbolEntity(TweetEntity symbolEntity) {
    if (symbolEntities == null) {
      symbolEntities = new ArrayList<>();
    }
    symbolEntities.add(symbolEntity);
  }
  
  public void addUrlEntity(TweetEntity urlEntity) {
    if (urlEntities == null) {
      urlEntities = new ArrayList<>();
    }
    urlEntities.add(urlEntity);
  }
  
  public void addUserMentionEntity(TweetEntity userMentionEntity) {
    if (userMentionEntities == null) {
      userMentionEntities = new ArrayList<>();
    }
    userMentionEntities.add(userMentionEntity);
  }
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public Long getTweetId() {
    return tweetId;
  }
  
  public void setTweetId(Long tweetId) {
    this.tweetId = tweetId;
  }
  
  public Date getTweetTimestamp() {
    return tweetTimestamp;
  }
  
  public void setTweetTimestamp(Date tweetTimestamp) {
    this.tweetTimestamp = tweetTimestamp;
  }
  
  public String getTweetLanguageCode() {
    return tweetLanguageCode;
  }
  
  public void setTweetLanguageCode(String tweetLanguageCode) {
    this.tweetLanguageCode = tweetLanguageCode;
  }
  
  public TweetCoordinates getTweetCoordinates() {
    return tweetCoordinates;
  }

  public void setTweetCoordinates(TweetCoordinates tweetCoordinates) {
    this.tweetCoordinates = tweetCoordinates;
  }

  public String getTweetText() {
    return tweetText;
  }
  
  public void setTweetText(String tweetText) {
    this.tweetText = tweetText;
  }
  
  public String getTweetTextWithoutStopWords() {
    return tweetTextWithoutStopWords;
  }

  public void setTweetTextWithoutStopWords(String tweetTextWithoutStopWords) {
    this.tweetTextWithoutStopWords = tweetTextWithoutStopWords;
  }

  public List<String> getLemmatizedTweetWords() {
    return lemmatizedTweetWords;
  }

  public void setLemmatizedTweetWords(List<String> lemmatizedTweetWords) {
    this.lemmatizedTweetWords = lemmatizedTweetWords;
  }

  public List<TweetEntity> getHashtagEntities() {
    return hashtagEntities;
  }
  
  public void setHashtagEntities(List<TweetEntity> hashtagEntities) {
    this.hashtagEntities = hashtagEntities;
  }
  
  public List<TweetEntity> getMediaEntities() {
    return mediaEntities;
  }
  
  public void setMediaEntities(List<TweetEntity> mediaEntities) {
    this.mediaEntities = mediaEntities;
  }
  
  public List<TweetEntity> getSymbolEntities() {
    return symbolEntities;
  }
  
  public void setSymbolEntities(List<TweetEntity> symbolEntities) {
    this.symbolEntities = symbolEntities;
  }
  
  public List<TweetEntity> getUrlEntities() {
    return urlEntities;
  }
  
  public void setUrlEntities(List<TweetEntity> urlEntities) {
    this.urlEntities = urlEntities;
  }
  
  public List<TweetEntity> getUserMentionEntities() {
    return userMentionEntities;
  }
  
  public void setUserMentionEntities(List<TweetEntity> userMentionEntities) {
    this.userMentionEntities = userMentionEntities;
  }
  
  public TweetUserData getUserData() {
    return userData;
  }
  
  public void setUserData(TweetUserData userData) {
    this.userData = userData;
  }
  
}
