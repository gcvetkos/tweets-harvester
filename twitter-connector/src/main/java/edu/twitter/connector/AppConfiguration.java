package edu.twitter.connector;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

import com.mongodb.MongoClient;

import edu.twitter.connector.config.DispatcherConfig;
import edu.twitter.connector.service.TweetHarvesterService;
import edu.twitter.connector.service.impl.TweetHarvesterServiceImpl;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages={"edu.twitter.connector"})
@Import({WebInitializer.class, DispatcherConfig.class})
@PropertySource({"classpath:app.properties"})
public class AppConfiguration {
  
  private static final Logger logger = LoggerFactory.getLogger(AppConfiguration.class);
  @Value("${twitter.OAuthConsumerKey}")
  private String authConsumerKey;
  @Value("${twitter.OAuthConsumerSecret}")
  private String authConsumerSecret;
  @Value("${twitter.OAuthAccessToken}")
  private String authAccessToken;
  @Value("${twitter.OAuthAccessTokenSecret}")
  private String authAccessTokenSecret;
  @Value("${mail.host}")
  private String host;
  @Value("${mail.username}")
  private String username;
  @Value("${mail.password}")
  private String password;
  @Value("${mail.smtp.auth}")
  private String smtpAuth;
  @Value("${mail.smtp.socketFactory.class}")
  private String socketFactoryClass;
  @Value("${mail.smtp.socketFactory.port}")
  private String socketFactoryPort;
  @Value("${mail.smtp.starttls.enable}")
  private String tlsEnable;
  @Value("${app.use.spring.data}")
  private String useSpringData;
  @Value("${app.db.name}")
  private String dbName;
  
  @Bean
  public TweetHarvesterService tweetsHarvester() {
    return new TweetHarvesterServiceImpl(Boolean.valueOf(useSpringData));
  }
  
  @Bean
  public MongoClient mongo() {
    MongoClient mongo = null;
    try {
      mongo = new MongoClient("localhost", 27017);
    } catch (UnknownHostException e) {
      logger.info("MongoDB Connection Error :" + e.getMessage());
    }
    logger.info("Mongo DB is online..");
    return mongo;
  }
  
  @Bean
  public twitter4j.conf.Configuration twitterConfiguration() {
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setDebugEnabled(true);

    cb.setOAuthConsumerKey(this.authConsumerKey);
    cb.setOAuthConsumerSecret(this.authConsumerSecret);
    cb.setOAuthAccessToken(this.authAccessToken);
    cb.setOAuthAccessTokenSecret(this.authAccessTokenSecret);
    return cb.build();
  }
  
  @Bean
  public TwitterStream twitterStream() {
    return new TwitterStreamFactory(twitterConfiguration()).getInstance();
  }
  
  @Bean
  public MongoDbFactory mongoDbFactory() throws Exception {
  return new SimpleMongoDbFactory(mongo(), dbName);
  }
  
  @Bean
  public MongoTemplate mongoTemplate() throws Exception {
    // remove _class
    DefaultDbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
    MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
    converter.setTypeMapper(new DefaultMongoTypeMapper(null));
    
    return new MongoTemplate(mongoDbFactory(), converter);
  }
  
  @Bean
  public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

}
