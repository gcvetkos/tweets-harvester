package edu.twitter.connector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.twitter.connector.model.TweetsQuery;
import edu.twitter.connector.service.TweetHarvesterService;


@Controller
@RequestMapping({"/"})
public class MainController {
  
  @Autowired
  private TweetHarvesterService tweetsHarvester;

  @RequestMapping(method = { RequestMethod.GET })
  public String main() {
    return "main";
  }

  @RequestMapping(value = { "/start" }, method = { RequestMethod.POST }, consumes = { "application/json" })
  public String startTwitterStream(@RequestBody TweetsQuery tweetsQuery) {
    this.tweetsHarvester.startTwitterStream(tweetsQuery);
    return "main";
  }

  @RequestMapping(value = { "/stop" }, method = { RequestMethod.POST })
  public String stopTwitterStream() {
    this.tweetsHarvester.stopTwitterStream();
    return "main";
  }

}
