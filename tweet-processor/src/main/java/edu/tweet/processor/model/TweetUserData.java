package edu.tweet.processor.model;

public class TweetUserData {
  
  private String userName;
  private int userFollowersCount;
  private int retweetCount;
  
  public String getUserName() {
    return userName;
  }
  
  public void setUserName(String userName) {
    this.userName = userName;
  }
  
  public int getUserFollowersCount() {
    return userFollowersCount;
  }
  
  public void setUserFollowersCount(int userFollowersCount) {
    this.userFollowersCount = userFollowersCount;
  }
  
  public int getRetweetCount() {
    return retweetCount;
  }
  
  public void setRetweetCount(int retweetCount) {
    this.retweetCount = retweetCount;
  }

}
