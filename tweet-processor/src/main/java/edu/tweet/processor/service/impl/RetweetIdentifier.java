package edu.tweet.processor.service.impl;

import org.springframework.stereotype.Service;

import edu.tweet.processor.model.Tweet;
import edu.tweet.processor.service.Transformer;

@Service
public class RetweetIdentifier implements Transformer<Tweet> {
  
  @Override
  public Tweet transform(Tweet tweet) {
    
    String tweetText = tweet.getTweetText().replaceAll("RT", "");
    tweet.setTweetText(tweetText);
    
    return tweet;
  }

}
