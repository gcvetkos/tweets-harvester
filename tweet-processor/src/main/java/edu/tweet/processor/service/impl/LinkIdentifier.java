package edu.tweet.processor.service.impl;

import static edu.tweet.processor.util.TweetRegex.VALID_URL;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.springframework.stereotype.Service;

import edu.tweet.processor.model.Tweet;
import edu.tweet.processor.model.TweetEntity;
import edu.tweet.processor.service.Transformer;

@Service
public class LinkIdentifier implements Transformer<Tweet> {

  @Override
  public Tweet transform(Tweet tweet) {
    List<TweetEntity> urls = extractUrls(tweet.getTweetText());
    tweet.setUrlEntities(urls);
    return tweet;
  }
  
  public List<TweetEntity> extractUrls(String input) {
    List<TweetEntity> result = new ArrayList<TweetEntity>();

    Matcher matcher = VALID_URL.matcher(input);
    while (matcher.find()) {
      TweetEntity url = new TweetEntity();
      url.setStart(matcher.start());
      url.setEnd(matcher.end());
      url.setText(matcher.group());
      result.add(url);
    }

    return result;
  }

}
