package edu.tweet.processor.service.impl;

import static edu.tweet.processor.util.TweetRegex.VALID_MENTION_OR_LIST;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.springframework.stereotype.Service;

import edu.tweet.processor.model.Tweet;
import edu.tweet.processor.model.TweetEntity;
import edu.tweet.processor.service.Transformer;

@Service
public class UserMentionIdentifier implements Transformer<Tweet> {

  @Override
  public Tweet transform(Tweet tweet) {
    
    List<TweetEntity> userMentions = extractUserMentions(tweet.getTweetText());
    tweet.setUrlEntities(userMentions);
    
    return tweet;
  }
  
  public List<TweetEntity> extractUserMentions(String input) {
    List<TweetEntity> result = new ArrayList<TweetEntity>();

    Matcher matcher = VALID_MENTION_OR_LIST.matcher(input);
    while (matcher.find()) {
      TweetEntity userMention = new TweetEntity();
      userMention.setStart(matcher.start());
      userMention.setEnd(matcher.end());
      userMention.setText(matcher.group());
      result.add(userMention);
    }

    return result;
  }


}
