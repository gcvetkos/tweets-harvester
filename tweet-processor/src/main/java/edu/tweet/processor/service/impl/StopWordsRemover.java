package edu.tweet.processor.service.impl;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import edu.tweet.processor.model.Tweet;
import edu.tweet.processor.service.Transformer;

/**
 * The proper implementation of this transformer should take all stop words from
 * the stopWords file. For now, we take the default lucene set of stop words.
 * For the implementation, look here: http://stackoverflow.com/questions/18008999/how-to-add-custom-stop-words-using-lucene-in-java
 */
@Service
public class StopWordsRemover implements Transformer<Tweet> {
  
  private static final Logger logger = LoggerFactory.getLogger(StopWordsRemover.class);

  @Override
  public Tweet transform(Tweet tweet) {
    CharArraySet stopWords = new CharArraySet(
        EnglishAnalyzer.getDefaultStopSet(), true);
    try (TokenStream tokenStream = new StopFilter(new StandardTokenizer(
        new StringReader(tweet.getTweetText().trim())), stopWords)) {
      StringBuilder sb = new StringBuilder();
      CharTermAttribute charTermAttribute = tokenStream
          .addAttribute(CharTermAttribute.class);
      tokenStream.reset();
      while (tokenStream.incrementToken()) {
        String term = charTermAttribute.toString();
        sb.append(term + " ");
      }
      tweet.setTweetTextWithoutStopWords(sb.toString());
    } catch (IOException e) {
      logger.error("Error while removing the stop words.", e);
    }

    return tweet;
  }

}
