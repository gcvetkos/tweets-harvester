package edu.tweet.processor.service;

public interface Transformer<E> {
  
  E transform(E entity);

}
