package edu.tweet.processor.util;

import java.util.regex.Pattern;

/**
 * Taken from https://github.com/twitter/twitter-text-java/blob/master/src/com/twitter/Regex.java. 
 */
public class TweetRegex {
  
  private static String AT_SIGNS_CHARS = "@\uFF20";
  public static final Pattern AT_SIGNS = Pattern.compile("[" + AT_SIGNS_CHARS + "]");
  public static final Pattern VALID_MENTION_OR_LIST = Pattern.compile("([^a-z0-9_!#$%&*" + AT_SIGNS_CHARS + "]|^|RT:?)(" + AT_SIGNS + "+)([a-z0-9_]{1,20})(/[a-z][a-z0-9_\\-]{0,24})?", Pattern.CASE_INSENSITIVE);
  
  public static final Pattern VALID_URL = Pattern.compile(
      "\\b(((ht|f)tp(s?)\\:\\/\\/|~\\/|\\/)|www.)" + 
      "(\\w+:\\w+@)?(([-\\w]+\\.)+(com|org|net|gov" + 
      "|mil|biz|info|mobi|name|aero|jobs|museum" + 
      "|travel|[a-z]{2}))(:[\\d]{1,5})?" + 
      "(((\\/([-\\w~!$+|.,=]|%[a-f\\d]{2})+)+|\\/)+|\\?|#)?" + 
      "((\\?([-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" + 
      "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)" + 
      "(&(?:[-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" + 
      "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)*)*" + 
      "(#([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)?\\b");
  
}
