package edu.tweet.processor.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.tweet.processor.model.Tweet;
import edu.tweet.processor.service.impl.HashtagIdentifier;
import edu.tweet.processor.service.impl.LinkIdentifier;
import edu.tweet.processor.service.impl.NewLineIdentifier;
import edu.tweet.processor.service.impl.RetweetIdentifier;
import edu.tweet.processor.service.impl.StopWordsRemover;
import edu.tweet.processor.service.impl.TweetLemmatizer;
import edu.tweet.processor.service.impl.UserMentionIdentifier;


/**
 * Handles requests from the twitter connector.
 */
@Controller
public class RestController {
	
	@Autowired
	private HashtagIdentifier hashtagIdentifier;
	@Autowired
	private LinkIdentifier linkIdentifier;
	@Autowired
	private UserMentionIdentifier userMentionIdentifier;
	@Autowired
	private RetweetIdentifier retweetIdentifier;
	@Autowired
	private NewLineIdentifier newLineIdentifier;
	@Autowired
	private StopWordsRemover stopWordsRemover;
	@Autowired
	private TweetLemmatizer tweetLemmatizer;

	@RequestMapping(value = "/rest/tweet/removeHashtags", method = RequestMethod.POST)
	public @ResponseBody
	Tweet removeHashtagsFromTweet(@RequestBody Tweet tweet) {
		return hashtagIdentifier.transform(tweet);
	}
	
	@RequestMapping(value = "/rest/tweet/removeLinks", method = RequestMethod.POST)
	public @ResponseBody
	Tweet removeLinksFromTweet(@RequestBody Tweet tweet) {
		return linkIdentifier.transform(tweet);
	}
	
	@RequestMapping(value = "/rest/tweet/removeUserMentions", method = RequestMethod.POST)
	public @ResponseBody
	Tweet removeUserMentionsFromTweet(@RequestBody Tweet tweet) {
		return userMentionIdentifier.transform(tweet);
	}
	
	@RequestMapping(value = "/rest/tweet/removeRetweet", method = RequestMethod.POST)
	public @ResponseBody
	Tweet removeRetweetSymbolFromTweet(@RequestBody Tweet tweet) {
		return retweetIdentifier.transform(tweet);
	}
	
	@RequestMapping(value = "/rest/tweet/removeNewLines", method = RequestMethod.POST)
	public @ResponseBody
	Tweet removeNewLinesFromTweet(@RequestBody Tweet tweet) {
		return newLineIdentifier.transform(tweet);
	}
	
	@RequestMapping(value = "/rest/tweet/removeStopWords", method = RequestMethod.POST)
	public @ResponseBody
	Tweet removeStopWordsFromTweet(@RequestBody Tweet tweet) {
		return stopWordsRemover.transform(tweet);
	}
	
	@RequestMapping(value = "/rest/tweet/lemmatizeTweet", method = RequestMethod.POST)
	public @ResponseBody
	Tweet lemmatizeTweet(@RequestBody Tweet tweet) {
		return tweetLemmatizer.transform(tweet);
	}

}
