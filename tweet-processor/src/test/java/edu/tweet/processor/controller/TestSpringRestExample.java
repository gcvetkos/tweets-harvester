package edu.tweet.processor.controller;

import org.springframework.web.client.RestTemplate;

import edu.tweet.processor.model.Tweet;

public class TestSpringRestExample {

	public static final String SERVER_URI = "http://localhost:7080/tweet-processor";
	
	public static void main(String args[]){
	  
//	  see some useful stuff here:
//	    http://www.baeldung.com/2011/10/25/building-a-restful-web-service-with-spring-3-1-and-java-based-configuration-part-2/
//	      http://java.dzone.com/articles/crud-using-spring-mvc-40
//	        http://howtodoinjava.com/2015/02/20/spring-restful-client-resttemplate-example/
		
	  testRemoveHashTags();
	}

	private static void testRemoveHashTags() {
		RestTemplate restTemplate = new RestTemplate();
		Tweet request = new Tweet();
		request.setTweetText("This is simple tweet with #one and #two hastags.");

		Tweet response = restTemplate.postForObject(SERVER_URI
				+ "/rest/tweet/removeHashtags", request, Tweet.class);
		System.out.println(response.getTweetText());
	}

}
