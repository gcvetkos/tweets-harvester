# TWEETS-HARVESTER#

Tweets-Harvester is a simple web application that:

* collects tweets from the Twitter streaming API
* performs some filter and transformation steps and
* saves the tweets in a Mongo DB.

The processing steps are separated in a REST web service and can be invoked separately. Currently the following processing steps are available:

* hashtag removal
* links removal
* user mentions removal
* retweet symbol removal
* new lines removal
* stop words removal
* tweet lemmatizer